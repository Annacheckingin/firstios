//
//  SceneDelegate.h
//  FastLane_IOS
//
//  Created by LiZhengGuo on 2021/10/10.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

